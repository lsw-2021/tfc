/*
    É um triângulo ou não é? Você deverá criar um programa que decida para o usuário se as
    três entradas de valores numéricos oferecidas por ele correspondem aos lados de triângulo
    ou não. As regras para validação de triângulos é tal que:
    Um conjunto de 3 valores numéricos são validados como lados de um triângulo se, somente
    se, um de seus lados for maior que o valor absoluto (módulo) da diferença entre os outros
    dois lados e menor que a soma dos outros dois lados:
*/


    let a = 5;
    let b = 4;
    let c = 3;
    
    if(a+b>c && b+c>a && c+a>b){
        console.log("É um triangulo!");
    }else{
        console.log("Não é um triangulo!")
    }