/**
 *  Na sala de aula de uma disciplina do IFTM há apenas três pessoas que optaram pelo pres-
    encial no ensino hı́brido. O professor decidiu mostrar o aproveitamento das atividades
    presenciais a partir do valor da média da nota final destes. Crie um programa que ajude
    o professor a criar um relatório para a coordenação no seguinte formato:
 */

    let nota_1 = 38.5;
    let nota_2 = 86.4;
    let nota_3= 60.2;

    let resp = (nota_1 + nota_2 + nota_3)/3;

    let saida = `
    O aluno 1 teve um aproveitamento de:${nota_1}
    O aluno 2 teve um aproveitamento de:${nota_2}
    O aluno 3 teve um aproveitamento de:${nota_3}
    A média da nota final de todos os alunos que frequentaram o presencial foi de:${resp}
    `;

    console.log(saida);