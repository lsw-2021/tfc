/*
Crie um programa que decida se um aluno foi aprovado ou não. Caso não seja aprovado,
o programa deverá mostrar quantos pontos o aluno precisaria para completar a média de
60 pontos. O programa tem quatro entradas que representam a pontuação do aluno em
quatro atividades elaboradas pelo professor. A primeira é de 15 pontos, a segunda de
25 pontos, a terceira de 25 pontos e a quarta de 35 pontos. O lançamento de notas não
pode ultrapassar a pontuação máxima de cada atividade. Exemplos de saı́da do programa:

*/
const nota_min = 60;
let nota_1 = 5;
let nota_2 = 23;
let nota_3 = 23;
let nota_4 = 35;

nota_total = (nota_1+nota_2+nota_3+nota_4);

if(nota_1 <0 || nota_1>15 ){
    console.log(`Erro: atividade 1 não pode ter mais que 15 pontos lançados!`);
}else if(nota_2 > 25 || nota_2 < 0 ){
    console.log(`Erro: atividade 2 não pode ter mais que 25 pontos lançados!`);
}else if(nota_3 > 25 || nota_3 < 0 ){
    console.log(`Erro: atividade 3 não pode ter mais que 25 pontos lançados!`);
}else if(nota_4 >35 || nota_4 <0){
    console.log(`Erro: atividade 4 não pode ter mais que 35 pontos lançados!`);
}else{
    if(nota_total > nota_min){
        console.log("O aluno foi aprovado :)")
    }else if(nota_total < nota_min){
        let temp = nota_total - nota_min;
        console.log(`O aluno foi reprovado, faltando ${temp} pontos para a pontuação mı́nima de aprovação`);
    }


}





