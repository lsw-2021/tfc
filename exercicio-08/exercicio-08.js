/*
      Vamos criar um jogo ao estilo Role Playing Game! Começaremos por definir o sistema
    de batalha e, para isso, você precisará desenvolver um programa com quatro entradas do
    tipo string e duas entradas do tipo numérico. Duas entradas (string) guardarão o nome
    dos adversários e as outras duas (string) suas respectivas armas. As entradas do tipo
    numérico irão guardar valores entre 1 e 100 representando os pontos de força de cada
    adversário. O jogo consistirá de apenas um turno e os adversário possuem 1800 pontos
    de vida cada. Ao executar o turno, um valor pseudoaleatório entre 1 e 20 deverá ser
    gerado. Este valor servirá como potencializador da força de ataque dos adversários. Os
    dois adversários irão atacar ao mesmo tempo no mesmo turno, os seus respectivos valores
    de força de ataque deverão se multiplicar ao fator potencializador aleatório somado ao
    fator de ataque da arma. O valor total deverá ser subtraı́do pelo fator de defesa da arma
    vezes 100. 
      *Arma*                *Ataque*      *Defesa*

      Martelo de Madeira     16           2
      Florete                10           7
      Arco e Flechas         20           0 
      Varinha de Inverno     17           1
    
    
    */

const random = Math.floor(Math.random() * (21 - 1) + 1);

let jogador_1 = "Cleber";
let arma_1 = "Martelo de Madeira";

let jogador_2 = "Mauricio";
let arma_2 = "Varinha de Inverno";

let pf_1 = 67;
let pf_2 = 15;

//valor aleatório
console.log("Valor aletorio: ", random);

let ataque_1;
let ataque_2;
if (pf_1 <= 100 && pf_1 >= 0 && pf_2 <= 100 && pf_2 >= 0) {

  switch (arma_1, arma_2) {
    case 'Martelo de Madeira', 'Florete':

      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_1 + 16) * random)-2*100-1800;
      //console.log("Valor de ataque: ", ataque_1);

      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 10) * random)-7*100-1800;
      //console.log("Valor de ataque: ", ataque_2);

      break;
    case 'Martelo de Madeira', 'Arco e Flexas':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_1 + 16) * random)-2*100-1800;

      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 20) * random)-0*100-1800;
      //console.log("Valor de ataque: ", ataque_2);
      break;

    case 'Martelo de Madeira', 'Varinha de Inverno':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_1 + 16) * random)-2*100-1800;

    
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 17) * random) - 1*100-1800;
      //console.log("Valor de ataque: ", ataque_2);
      break;

    case 'Martelo de Madeira', 'Martelo de Madeira':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_1 + 16) * random)-2*100-1800;

      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_1 = ((pf_1 + 16) * random)-2*100-1800;
      //console.log("Valor de ataque: ", ataque_2);

      break;

    case 'Florete', 'Arco e flechas':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_2 + 10) * random)-7*100-1800;

      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 20) * random)-0*100-1800;
      //console.log("Valor de ataque: ", ataque_2);

      break;
    case 'Florete', 'Varinha de Inverno':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_2 + 10) * random)-7*100-1800;
      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 17) * random) - 1*100-1800;
      //console.log("Valor de ataque: ", ataque_2);

      break;
    case 'Florete', 'Florete':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_2 + 10) * random)-7*100-1800;

      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 10) * random)-7*100-1800;
      //console.log("Valor de ataque: ", ataque_2);
      break;
    case 'Arco e flechas', 'Varinha de Inverno':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_2 + 20) * random)-0*100-1800;

      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 17) * random) - 1*100-1800;
      //console.log("Valor de ataque: ", ataque_2);

      break;
    case 'Arco e flechas', 'Arco e Flechas':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_2 + 20) * random)-0*100-1800;

      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 20) * random)-0*100-1800;
      //console.log("Valor de ataque: ", ataque_2);

      break;
    case 'Varinha de Inverno', 'Varinha de Inverno':
      //Ataque e Defesa Jogador 1
      ataque_1 = ((pf_2 + 17) * random) - 1*100-1800;

      //console.log("Valor de ataque: ", ataque_1);
      //Ataque e Defesa Jogador 2
      ataque_2 = ((pf_2 + 17) * random) - 1*100-1800*(-1);
      //console.log("Valor de ataque: ", ataque_2);

    break;
      
  }

  let resultado;
  if(Math.abs(ataque_1) > 1800){
    resultado = `${jogador_2} morreu.`;
  }else if(Math.abs(ataque_2)>1800){
    resultado = `${jogador_1} morreu.`;
  }else if(Math.abs(ataque_1) && Math.abs(ataque_1) < 1800){
    resultado = `Todos sobreviveram.`;
  }else{
    resultado = `todos morreram.`;
  }
  


  
  let saida=`${jogador_1} perdeu ${Math.abs(ataque_1)} pontos de vida no ataque de ${jogador_2}.
    ${jogador_2} perdeu ${Math.abs(ataque_2)} pontos de vida no ataque de ${jogador_1}.
    ${resultado}
    `;

    console.log(saida);
} else {
  console.log("Jogadores, revisem seus pontos de força");
}